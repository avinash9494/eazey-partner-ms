package com.eazey.partner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EazeyPartnerMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EazeyPartnerMsApplication.class, args);
	}

}
