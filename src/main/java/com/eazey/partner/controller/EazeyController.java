package com.eazey.partner.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EazeyController {
	
	@GetMapping("/health")
	public String health() {
		return "Application is up and running!!!";
	}

}
