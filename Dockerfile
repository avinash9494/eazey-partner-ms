# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy jar into image
ADD /target/eazey-partner-ms-0.0.1-SNAPSHOT.jar eazeyuser.jar
# run application with this command line 
CMD ["/usr/bin/java", "-jar", "eazeyuser.jar"]
